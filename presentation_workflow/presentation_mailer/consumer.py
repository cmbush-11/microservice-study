import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f'{presentation["presenter_name"]}, we are happy to tell you that your presentation {presentation["title"]} has been accepted',
        "admin@conferencego.com",
        [f'{presentation["presenter_email"]}'],
        fail_silently=False,
    ),


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.start_consuming()


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f'{presentation["presenter_name"]}, we are sorry to tell you that your presentation {presentation["title"]} has been rejected',
        "admin@conferencego.com",
        [f'{presentation["presenter_email"]}'],
        fail_silently=False,
    ),


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()


#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue="tasks")
#     channel.basic_consume(
#         queue="presentation_approvals",
#         on_message_callback=send_mail(
#             "Your presentation has been accepted",
#             "{presentation.presenter_name}, we're happy to tell you that your presentation {[presentation.title]} has been accepted",
#             "admin@conferencego.com",
#             ["presentation.presenter_email"],
#             fail_silently=False,
#         ),
#         auto_ack=True,
#     )
#     channel.start_consuming()
